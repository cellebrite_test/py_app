from prometheus_client import start_http_server
from prometheus_client.core import GaugeMetricFamily, REGISTRY, Metric
import urllib2
from time import sleep
import lxml.html as lh
import json
import sys, errno#, socket, os
from datetime import datetime

LIST_OF_ENVS = ["aubquk03", "aucafr03"]
HTTP_SERVER_DEFAULT_PORT = 8056
MOCK_DEFAULT_STATUSES_FILE = 'files/MockDefaultStatuses.json'
MOCK_LOCAL_HTML_SUFFIX = "MockPage.html"


class MockStatusCollector(object):
  """
    Agent Mock statuses exporter class
  """

  def __init__(self):
    try:
      with open (MOCK_DEFAULT_STATUSES_FILE, 'rt') as mf:
        self._mockStatusDefaults = json.loads(mf.read())
    except IOError:
      raise IOError("{0} not found".format(MOCK_DEFAULT_STATUSES_FILE))


  def collect(self):
    """
      Collects Agent Mock statuses. 
      Returns metrics 
    """
    metric = GaugeMetricFamily('atgapi_agent_mock_status', '.COM+ ATGAPI Agent Mock status state', labels=['envName', 'mockName', 'mockStatus'])
    for (envName, statuses) in self._getMockStatusesFile().items():
      #print envName, "\t", statuses.keys(), "\t", type(statuses), "\t", len(statuses),"\n", 
      for (mockName, status) in statuses.items():
        try:
          #print "envName: {0} \t mockName: {1} \t status: {2} \n".format(envName, mockName, status)
          #print "MockName: {0} \t Current status: {1} \t Default status: {2}".format(mockName, self._mockStatusDefaults[envName][mockName] ,status)
	  if envName not in self._mockStatusDefaults.keys():
            mockStatusMatch = 0
	  elif status == 'StatusUnknown':
	    mockStatusMatch = 504
          elif self._mockStatusDefaults[envName][mockName] == status: 
            mockStatusMatch = 0
          elif self._mockStatusDefaults[envName][mockName] != status:
            mockStatusMatch = 1
          else:
            mockStatusMatch = 99

          metric.add_metric(labels=[envName, mockName, status], value=mockStatusMatch)
        except KeyError as err:
          print "KeyError happened in collect method:", err, "\t mockName: ", mockName, " status: ", status
          metric.add_metric(labels=[envName, mockName, status], value=600)
          pass
      print "[{0}] \tMetrics added for: {1}".format(datetime.now(), envName)
    yield metric


  def _getMockStatusesURL(self):
    """
      Request AGENT_MOCK_URL* for all specified environments in LIST_OF_ENVS.
      Return dict of dicts with mock statuses for all environments.
      *AGENT_MOCK_URL: http://atg-$ENVNAME-aws-afa01.aws.com:8030/dyn/admin/nucleus/kf/commerce/mocks/MockConfiguration
    """
    mockAllEnvs = {}
    for envName in LIST_OF_ENVS:
      mockConfUrl = "http://atg-" + envName + "-aws-afa01.aws.com:8030/dyn/admin/nucleus/kf/commerce/mocks/MockConfiguration"
      print mockConfUrl
      try:
        request = urllib2.urlopen(mockConfUrl, timeout = 3)
        contents = request.read()
        print "Response code: \t", request.code
        mockStatus = self._parseHtmlContents(contents)
        mockAllEnvs.update({envName: mockStatus})
	print "Gathered"
      except urllib2.URLError, err:
        print "Error urllib2.URLError happened:", err.reason
        print "Response code: \t", err.args
	print "Not gathered"
        mockAllEnvs.update({envName: dict({"URLRequestTimout": "StatusUnknown"})})
        pass
      except urllib2, err:
        #print "Error urllib2 happened:", sys.exc_info()[0]
        mockAllEnvs.update({envName: dict({"urllib2.err": "StatusUnknown"})})
        pass
      except:
        print "Unexpected error:", sys.exc_info()[0]
        pass
    return mockAllEnvs


  def _getMockStatusesFile(self):
    """
      Reads local html files(naming: $envName+"MockPage.html") for all specified environments in LIST_OF_ENVS.
      Return dict of dicts with mock statuses for all environments.
    """
    mockAllEnvs = {}
    for envName in LIST_OF_ENVS:
      fileName = "files/" + envName + MOCK_LOCAL_HTML_SUFFIX
      with open (fileName, 'rt') as mf:
        contents = mf.read()
      mockStatus = self._parseHtmlContents(contents)
      mockAllEnvs.update({envName: mockStatus})
    return mockAllEnvs


  def _parseHtmlContents(self, htmlContent):
    """
      Parse received html content - look for all <tr> tags. 
      Return dict object with all rows matches "boolean" and "mock" string values from received html content 
    """
    MockStatusDict = {}
    try:
      doc = lh.fromstring(htmlContent)
      tr_elements = doc.xpath('//tr')
      for name,value,typ in tr_elements:
        if typ.text_content() == 'boolean' and 'mock' in name.text_content():
          MockStatusDict.update({name.text_content(): value.text_content()})
    except ValueError as ve:
      pass
    return MockStatusDict
  

  def _getDefaultMockStatusesJSONFile(self):
    """
      Match json
    """
    with open (MOCK_DEFAULT_STATUSES_FILE, 'rt') as mf:
      contents = mf.read()
    jsonObj = json.loads(contents)
    #print contents, "\n", type(contents), "\n", type(jsonObj)
    return jsonObj


def main():
  """
    Main. Start http server and register MockStatusCollector. 
    Exposes metrics
  """
#  cl = MockStatusCollector()
#  for (envName, statuses) in cl._getMockStatusesFile().items():
#    for mockName, status in statuses.items():
#      print envName, "\t", mockName, "\t" ,status

  try:
    print " "
    start_http_server(HTTP_SERVER_DEFAULT_PORT)
    REGISTRY.register(MockStatusCollector())
    while True: sleep(3600)
#  except BrokenPipeError:
#    devnull = os.open(os.devnull, os.O_WRONLY)
#    os.dup2(devnull, sys.stdout.fileno())
#   sys.exit(1)
#    pass
  except KeyboardInterrupt:
    print(" Interrupted")
    sys.exit(0)
  else:
    sys.exit(1)


if __name__ == '__main__':
  main()



