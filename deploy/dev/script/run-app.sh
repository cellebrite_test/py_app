#!/bin/bash

DOCKER_REGISTRY=$(cat /tmp/config/DOCKER_REGISTRY)
APP_REVISION=$(cat /tmp/config/APP_REVISION)

docker run --name py_app -d -p 8056:8056 $DOCKER_REGISTRY/$APP_REVISION