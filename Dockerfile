FROM python:2.7.18-slim-buster

ENV PYTHONUNBUFFERED=1
ENV PYTHONWARNINGS="ignore:Unverified HTTPS request"

RUN STATIC_DEPS=true pip install lxml
COPY requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt

RUN useradd --create-home app
WORKDIR /home/app
USER app

COPY --chown=app:app src/files ./files

COPY --chown=app:app src/mockstatus_exporter.py ./

EXPOSE 8056

CMD ["python", "./mockstatus_exporter.py"]

