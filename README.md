# Py App

Prometheus exporter which is created to collect the state of mock features for the Oracle ATG(API component). 
The exporter reads the code from the HTML page, parses and returns it as metrics.
Exposed on port - 8056

CI/CD orchestrated by gitlab CI

# Build process 
The project is builded on a local gitlab-runner installed in the VirtualBox(CentOS7). 
- Install runner - https://docs.gitlab.com/runner/install/linux-manually.html 
- Register runner as docker executor
- Add "privileged = true" to /etc/gitlab-runner/config.toml

# Deploy process
The deployment process uses AWS CodeDeploy.
After the successful build stage of the image from the gitlab runner, commands are executed that call the CodeDeploy
